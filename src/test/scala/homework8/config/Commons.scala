package homework8.config

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.Date

import scala.util.Random


object Commons {

  def randomDate = {
    val a: Long = Random.nextInt(86400000)
    val b: Long = Random.nextInt(36500)
    val long: Long = Math.abs(a * b) + (18 * 366 * 24 * 3600 * 1000L)
    val date = new Date(System.currentTimeMillis() - long)
    val df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss")
    df.format(date)
  }

  def dateGenerator: String = {
    val startDate = LocalDate.now().minusYears(5)
    val endDate = LocalDate.now()
    val df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss")
    df.format(LocalDate.ofEpochDay(startDate.toEpochDay + Random.nextInt((endDate.toEpochDay - startDate.toEpochDay).toInt)))

  }
}