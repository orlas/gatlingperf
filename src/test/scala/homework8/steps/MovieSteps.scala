package homework8.steps

import homework8.model.Movie
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object MovieSteps extends StepsBase {

  def addMovie(movie: => Movie) = {
    http("Add movie")
      .post("/movies")
      .body(StringBody(session => writeJsonFromObject(movie)))
      .check(jsonPath("$.id").ofType[Int].saveAs("movieID"))
      .check(jsonPath("$.genre").saveAs("moveGenre"))
      .asJSON
      .check(status.is(200))
  }

  def getMoviesWithStatusAtribute(stat: => Int) = http("Get movies")
    .get("/movies")
    .check(status.is(stat))

  def getMoviesWithPageAtribute(page: => Int) = http("Get movies with page")
    .get(_ => s"/movies?page=?${page}")
    .check(status.is(200))

  def getMoviesByGenre(moveGenre: => String) = http("Get movie title by genie")
    .get("/movies/search?genre=${" + moveGenre + "}")
    .check(status.is(200))
    .check(jsonPath("$.genre").is("${" + moveGenre + "}"))
}


