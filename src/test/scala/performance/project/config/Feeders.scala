package performance.project.config

import io.gatling.core.Predef._


object Feeders {

  val arrayFeeder = Array(
    Map("param" -> 1, "param2" -> 2)
  )

  val usersFeeder = jsonFile("/home/rico/Dokumenty/GatlingPerf/src/test/resources/users.json").circular

}
