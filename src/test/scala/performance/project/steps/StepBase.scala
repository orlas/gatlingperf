package performance.project.steps

import io.gatling.core.Predef._
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization._

import scala.reflect.classTag

abstract class StepBase {

  implicit val formats = DefaultFormats

  def jsonBodyFromObject(obj: => AnyRef) = StringBody(_ => write(obj))

  def readJsonBodyAs[T](jsonString: String)(implicit m: Manifest[T]) = try {
    read[T](jsonString)
  } catch {
    case e: Exception => throw new Exception(s"Cannot deserialize ${classTag[T]} from \n${jsonString}\n " + e.getMessage)
  }
}
