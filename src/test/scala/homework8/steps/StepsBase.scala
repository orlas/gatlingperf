package homework8.steps

import scala.reflect.classTag

abstract class StepsBase {

  import org.json4s.DefaultFormats
  import org.json4s.jackson.Serialization.{read, write}

  implicit val formats = DefaultFormats

  def writeJsonFromObject(obj: => AnyRef) = write(obj)

  def readJsonBodyAs[T](jsonString: String)(implicit m: Manifest[T]) = try {
    read[T](jsonString)
  } catch {
    case e: Exception => throw new Exception(s"Cannot deserialize ${classTag[T]} from \n${jsonString}\n" + e.getMessage)
  }

}
