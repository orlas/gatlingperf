package performance.project.model

case class AuthorizationRequest(
                                 username: String,
                                 password: String
                               )
