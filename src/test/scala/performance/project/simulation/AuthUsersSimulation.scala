package performance.project.simulation

import io.gatling.core.Predef._
import performance.project.config.{CinemaAssertions, CinemaProfiles, CinemaProtocols, Feeders}
import performance.project.model.AuthorizationRequest
import performance.project.steps.UserSteps

class AuthUsersSimulation extends Simulation {

  val scn = scenario("Auth user")
    .feed(Feeders.usersFeeder)
    .exec(
      UserSteps.authenticateUser(AuthorizationRequest("${username}", "${password}"))
    )
    .exec(
      UserSteps.authenticateUserAlt
    )

  setUp(scn.inject(CinemaProfiles.constantWithWarmUp(1, 1)).protocols(CinemaProtocols.loadBalancer)).assertions(CinemaAssertions.globalAssertions)

}
