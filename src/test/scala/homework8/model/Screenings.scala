package homework8.model

import homework8.config.{Commons, Feeders}

import scala.util.Random


case class Screenings(
                       movieid: Int,
                       city: String,
                       roomnumber: Int,
                       seatslimit: Int,
                       seatstaken: Int,
                       time: String,
                       price: BigDecimal
                     )

object ScreeningsDataProvider {

  def movieScreening(movieID: Int) = Screenings(
    movieID,
    Feeders.randomCity,
    Random.nextInt(6),
    Random.nextInt(30),
    Random.nextInt(30),
    Commons.dateGenerator,
    BigDecimal.apply(15)
  )
}



