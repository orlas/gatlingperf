package homework8.config

import scala.util.Random

object Feeders {

  def randomCity = Random.shuffle(List("Paris", "London", "Cracow", "Berlin", "Amsterdam", "Madrid", "Rome", "Lizbon", "Moscow", "Tokyo")).head

  def randomName = Random.shuffle(List("Adam", "Edward", "Tom", "Sam", "Agata", "Anna", "Felicia", "Bill", "David", "Cate")).head

  def randomSurname = Random.shuffle(List("Smith", "Brown", "Bell", "Perry", "Baker", "Moore", "Cooper", "Powell", "Cox", "Nelson")).head

  def randomStatus = Random.shuffle(List("On", "Off", "Waiting")).head

}
