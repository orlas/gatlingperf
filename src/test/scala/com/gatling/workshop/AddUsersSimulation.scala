package com.gatling.workshop

import java.text.SimpleDateFormat
import java.util.Date

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.util.Random

class AddUsersSimulation extends Simulation {

  def randomName = Random.shuffle(List("Adam", "Edward", "Tom", "Sam", "Agata", "Anna", "Felicia", "Bill", "David", "Cate")).head

  def randomSurname = Random.shuffle(List("Smith", "Brown", "Bell", "Perry", "Baker", "Moore", "Cooper", "Powell", "Cox", "Nelson")).head

  def randomCity = Random.shuffle(List("Paris", "London", "Cracow", "Berlin", "Amsterdam", "Madrid", "Rome", "Lizbon", "Moscow", "Tokyo")).head

  def randomAddress = Random.shuffle(List("Oak street", "Lake street", "Hill avenue", "Pine street", "Park avenue")).head + s" ${Random.nextInt(100) + 1}"

  def randomDate = {
    val a: Long = Random.nextInt(86400000)
    val b: Long = Random.nextInt(36500)
    val long: Long = Math.abs(a * b) + (18 * 366 * 24 * 3600 * 1000L)
    val date = new Date(System.currentTimeMillis() - long)
    val df = new SimpleDateFormat("yyyy-MM-dd")
    df.format(date)
  }

  def userData(username: String, email: String, password: String) =
    s"""{
       |     "firstname":"${randomName}",
       |   "lastname":"${randomSurname}",
       |   "email": "${email}",
       |   "username":"${username}",
       |   "password":"${password}",
       |   "city":"${randomCity}",
       |   "address":"${randomAddress}",
       |   "birthdate":"${randomDate}"
       |}""".stripMargin

  val addUsersScenario = scenario("Add bunch of new users")
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("shadow", "shadow@mail.com", "asd123")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("kyle", "kyle@mail.com", "ter537")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("epic", "epic@mail.com", "nju453")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("zyky", "zyky@mail.com", "plu897")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("balon", "balon@mail.com", "erd234")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("uzi", "uzi@mail.com", "bng345")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("poster", "poster@mail.com", "bhy534")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("hanzo", "hanzo@mail.com", "plt645")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("doom", "doom@mail.com", "bht543")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("rosen", "rosen@mail.com", "uyj658")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("dust", "dust@mail.com", "hyti54")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("swag", "swag@mail.com", "bht59o")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("zoom", "zoom@mail.com", "lmu7y6")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("hellboy", "hellboy@mail.com", "vft53r")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("smart", "smart@mail.com", "gth65s")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("mist", "mist@mail.com", "gth654")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("lion", "lion@mail.com", "oku654")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("camper", "camper@mail.com", "jui65r")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("grant", "grant@mail.com", "jut654")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("lucky", "lucky@mail.com", "jny65s")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("speeder", "speeder@mail.com", "kin99u")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("dolly", "dolly@mail.com", "vgt12f")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("walker", "walker@mail.com", "bj76rt")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("hyper", "hyper@mail.com", "vg6543")))
        .asJSON
        .check(status.is(201))
    )
    .exec(
      http("Add user")
        .post("/users")
        .body(StringBody(_ => userData("frozen", "frozen@mail.com", "mkg541")))
        .asJSON
        .check(status.is(201))
    )
  val ip = Seq(atOnceUsers(1))
  val protocol = http.baseURLs("http://10.10.0.101:9000")
  setUp(addUsersScenario.inject(ip)).protocols(protocol)

}
