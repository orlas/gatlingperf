package com.gatling.workshop

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization._

import scala.concurrent.duration._
import scala.reflect.io.File
import scala.util.Random


class AddMovieSimulation extends Simulation with CustomChecks {

  override implicit val formats = DefaultFormats

  val addMovie = scenario("Add movie")
    .exec(session => session.set("movie", Movie()))
    .exec(
      http("Add movie request")
        .post("/movies")
        .body(StringBody(_ => {
          write(Movie())
        }))
        .asJSON
        .check(bodyString.transform(s => {
          read[Movie](s).agelimit >= 100
        }).is(true))
        .check(bodyString.transform(s => {
          read[Movie](s).director == "KKKKKKKKKK"
        }).is(true))
        .check(bodyString.transform((str, session) => {
          read[Movie](str).copy(id = None).equals(session("movie").as[Movie])
        }).is(true))
        .check(isAmerican)
    )

  //val isAmerican = bodyString.transform(str => read[Movie](str).country == "USA").is(true)

  val protocol = http.baseURL("http://10.10.0.101:9000").extraInfoExtractor(e => {
    File(s"simulation_" +
      s"${e.session.scenario}.log").appendAll(s"${e.request.getUrl} ${e.response.statusCode} ${e.response.timings.responseTime} \n",
      s"${e.response.body.string}\n")

    List.empty
  })

  setUp(addMovie.inject(constantUsersPerSec(1).during(5 seconds)).protocols(protocol))

}

case class Movie(
                  id: Option[Int] = None,
                  title: String = Random.alphanumeric.take(10).mkString,
                  director: String = Random.alphanumeric.take(10).mkString,
                  genre: String = Random.alphanumeric.take(10).mkString,
                  country: String = Random.alphanumeric.take(10).mkString,
                  year: Int = Random.nextInt(50) + 1950,
                  tags: String = Random.alphanumeric.take(10).mkString,
                  agelimit: Int = Random.nextInt(16) + 1
                )

trait CustomChecks extends Simulation {
  implicit val formats = DefaultFormats
  val isAmerican = bodyString.transform(str => read[Movie](str).country == "USA").is(true)
}