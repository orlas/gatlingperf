package homework8.model

import homework8.config.Feeders

import scala.util.Random

case class Orders(
                   id: Option[Int],
                   userid: Int,
                   screeningid: Int,
                   status: String,
                   ticketscount: Int,
                   totalprice: BigDecimal
                 )

object orderDataProvider {
  def orders(userId: Int, screeningId: Int) = Orders(
    None,
    userId,
    screeningId,
    Feeders.randomStatus,
    Random.nextInt(4),
    BigDecimal.apply(Random.nextInt(12))
  )
}
