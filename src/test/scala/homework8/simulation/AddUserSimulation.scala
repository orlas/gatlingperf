package homework8.simulation

import homework8.config.{Assertions, PerformanceProfiles, Protocol}
import homework8.model.UserDataProvider
import homework8.steps.UserSteps
import io.gatling.core.Predef._
import io.gatling.core.controller.inject.InjectionStep
import io.gatling.core.protocol.Protocol

class AddUSerViaLB extends AddUserSimulation(
  PerformanceProfiles.constWarmUpProfile(3, 30),
  Protocol.loadBalancerAddress,
  Assertions.globalAssertions
)

abstract class AddUserSimulation(
                                  ip: Seq[InjectionStep],
                                  prot: Protocol,
                                  assert: Seq[Assertion]
                                ) extends Simulation {

  val scn = scenario("Add user")
    .exec(
      UserSteps.addUserAndSaveUserParams(UserDataProvider.randomUser, "userSessionAttribute")
    )
    .exec(
      UserSteps.authenticateUserAndSaveTokenAndId("userSessionAttribute")
    )
    .exec(
      UserSteps.getUserOrdersAndSaveToFile("$userId", "$userToken")
    )
    .exec(
      UserSteps.payInUserWallet("$userId", "$userToken", 12)
    )



  setUp(scn.inject(ip).protocols(prot)).assertions(assert)
}

