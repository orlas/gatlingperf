package com.gatling.workshop

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class MoviesSimulation extends Simulation {

  //1. scenariusz

  val scn = scenario("Simulate html page")
    .group("group 1") {
      exec(http("g1-1").get("/"))
        .exec(http("g1-2").get("/"))
        .exec(http("g1-3").get("/"))
    }

    .exec(http("Get html")
      .get("/")
      .check(status.is(200))
      .check(css("div#main").exists)
    )
    .pause(5 seconds)
    .exec(http("get json")
      .get("http://10.10.0.100:9000/movies/1")
      .check(status.is(200))
      .check(jsonPath("$.title").is("The Hobbit: An Unexpected Journey"))
    )


  //2. injection profile

  val ip = constantUsersPerSec(5).during(10 seconds)

  //3. konfiguracja protokołu

  val protocol = http.baseURL("http://10.10.0.200")

  //4. asercje

  val assertions = Seq(
    global.responseTime.mean.lt(500),
    forAll.responseTime.mean.lt(500),
    details("group 1 / g1-1").responseTime.mean.lt(25))

  setUp(
    scn.inject(ip)
  ).protocols(protocol).assertions(assertions)



}
