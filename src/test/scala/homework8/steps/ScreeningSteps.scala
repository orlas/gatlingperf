package homework8.steps

import homework8.model.ScreeningsDataProvider
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object ScreeningSteps extends StepsBase {

  def addMoviesScreening(movieID: String) =
    http("Add screening")
      .post("/screening")
      .body(StringBody(session => writeJsonFromObject(ScreeningsDataProvider.movieScreening(session(movieID).as[Int]))))
      .asJSON
}
