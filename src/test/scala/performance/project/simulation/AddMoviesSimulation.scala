package performance.project.simulation

import io.gatling.core.Predef._
import io.gatling.core.controller.inject.InjectionStep
import io.gatling.core.protocol.Protocol
import performance.project.config.{CinemaAssertions, CinemaProfiles, CinemaProtocols}
import performance.project.model.MovieDataProvider
import performance.project.steps.MovieSteps

class AddMovieViaLB extends AddMoviesSimulation(
  CinemaProfiles.constantWithWarmUp(1, 2),
  CinemaProtocols.loadBalancer,
  CinemaAssertions.globalAssertions
)

abstract class AddMoviesSimulation(ip: Seq[InjectionStep], prot: Protocol, assert: Seq[Assertion]) extends Simulation {

  val scn = scenario("Add movie")
    .exec(
      MovieSteps.addMovie(MovieDataProvider.randomMovie)
    )

  setUp(
    scn
      .inject(ip)
      .protocols(prot))
    .assertions(assert)
}
