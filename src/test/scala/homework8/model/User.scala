package homework8.model

import homework8.config.{Commons, Feeders}

import scala.util.Random

case class User(
                 firstname: String,
                 lastname: String,
                 email: String,
                 username: String,
                 city: String,
                 address: String,
                 birthdate: String,
                 password: String,
                 token: Option[String]
               )

object UserDataProvider {
  def randomUser = User(
    Feeders.randomName,
    Feeders.randomSurname,
    s"${Random.nextInt(100000)}@gmail.com",
    Random.alphanumeric.take(50).mkString,
    Feeders.randomCity,
    s"${Feeders.randomCity}" + Random.nextInt(50),
    Commons.randomDate,
    Random.alphanumeric.take(30).mkString,
    None
  )
}
