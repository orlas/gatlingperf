package homework8.model

case class AuthorizationRequest(
                                 username: String,
                                 password: String
                               )
