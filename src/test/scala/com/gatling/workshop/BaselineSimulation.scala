package com.gatling.workshop

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._


class BaselineSimulation extends Simulation {

  val titles = Seq("First Movie Title", "Second Movie Title", "Third movie title")
  val directors = Seq("Jan Nowak", "Bill Smith", "Adam Kowalsky")
  val genres = Seq("Horror", "Comedy", "Drama")


  val scn = scenario("Get movies baseline")
    .during(3 seconds) {
      exec(http("Get all movies")
        .get("/movies/1")
        .check(
          status.is(200)
        )
      ).pause(50 millisecond)
    }

  val ip = Seq(
    rampUsersPerSec(1).to(15).during(15 seconds),
    constantUsersPerSec(15).during(30 seconds)
  )

  val protocol = http.baseURLs(
    "http://10.10.0.101:9000",
    "http://10.10.0.102:9000",
    "http://10.10.0.103:9000"
  )
    // .inferHtmlResources(white = WhiteList("http://10.10.0.200.*.jpg"))
    .inferHtmlResources(black = BlackList("http://10.10.0.200.*.jpg"))

  setUp(scn.inject(ip))
    .protocols(protocol)
    .throttle(//włącza ogranicznei ilości zapytań na sekundę przez Gatlinga, ma 3 opcje
      reachRps(400).in(240 seconds),
      jumpToRps(10), //maksmalnie 10 userów
      holdFor(15 seconds)
    ).maxDuration(45 seconds)

}
