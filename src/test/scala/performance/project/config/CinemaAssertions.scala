package performance.project.config

import io.gatling.core.Predef._

object CinemaAssertions {
  val globalAssertions = Seq(global.responseTime.max.lt(1000))

  val slowAssertion = Seq(
    global.responseTime.max.lt(5000),
    global.responseTime.percentile4.lt(3000)
  )

}
