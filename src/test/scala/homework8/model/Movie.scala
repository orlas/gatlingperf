package homework8.model

import scala.util.Random

case class Movie(
                  id: Option[Int] = None,
                  title: String = Random.alphanumeric.take(10).mkString,
                  director: String = Random.alphanumeric.take(10).mkString,
                  genre: String = Random.alphanumeric.take(10).mkString,
                  country: String = Random.alphanumeric.take(10).mkString,
                  year: Int = Random.nextInt(50) + 1950,
                  tags: String = Random.alphanumeric.take(10).mkString,
                  agelimit: Int = Random.nextInt(16) + 1
                )

object MovieDateProviderSec {
  def randomMovie = Movie()
}

