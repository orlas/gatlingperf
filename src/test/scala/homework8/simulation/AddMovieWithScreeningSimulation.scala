package homework8.simulation

import homework8.config.{Assertions, PerformanceProfiles, Protocol}
import homework8.model.MovieDateProviderSec
import homework8.steps.{MovieSteps, ScreeningSteps}
import io.gatling.core.Predef._
import io.gatling.core.controller.inject.InjectionStep
import io.gatling.core.protocol.Protocol


class AddMovieWithScreeningSimulationLB extends AddMovieWithScreeningSimulation(
  PerformanceProfiles.constWarmUpProfile(1, 10),
  Protocol.loadBalancerAddress,
  Assertions.globalAssertions
)

class AddMovieWithScreeningSimulationGateway extends AddMovieWithScreeningSimulation(
  PerformanceProfiles.constWarmUpProfile(1, 10),
  Protocol.gatewayAddressWithHeader,
  Assertions.slowAssertion
)

abstract class AddMovieWithScreeningSimulation(
                                                ip: Seq[InjectionStep],
                                                prot: Protocol,
                                                assert: Seq[Assertion]) extends Simulation {

  val scn = scenario("Add movie and screening")
    .exec(
      MovieSteps.addMovie(MovieDateProviderSec.randomMovie))
    .exec(
      ScreeningSteps.addMoviesScreening("movieID")
    )
    .exec(
      MovieSteps.getMoviesByGenre("moveGenre")
    )


  setUp(scn.inject(ip)).protocols(prot).assertions(assert)

}
