package homework8.config

import io.gatling.core.Predef.{constantUsersPerSec, _}

import scala.concurrent.duration._

object PerformanceProfiles {

  def constWarmUpProfile(users: Int, timeDuration: Int) = Seq(
    rampUsersPerSec(1).to(users).during(60 seconds),
    constantUsersPerSec(users).during(timeDuration seconds)
  )

  def rampUserWithPause(doNothingTime: Int, atUsers: Int, rampUser: Int, duration: Int) = Seq(
    nothingFor(doNothingTime.seconds),
    atOnceUsers(atUsers),
    rampUsers(rampUser) over (duration.seconds)
  )



}
