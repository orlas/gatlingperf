package performance.project.simulation

import io.gatling.core.Predef._
import io.gatling.core.protocol.Protocol
import performance.project.config.{CinemaAssertions, CinemaProfiles, CinemaProtocols}
import performance.project.steps.MovieSteps

//lass GetMoviesSimulationViaLB extends GetMoviesSimulation with LB

abstract class GetMoviesSimulation extends Simulation {

  val protocol: Protocol

  val scn = scenario("get movie")
    .exec(
      MovieSteps.getMovies
    )

  setUp(
    scn.inject(CinemaProfiles.oneDuringOne).protocols(CinemaProtocols.loadBalancer)
  ).assertions(CinemaAssertions.globalAssertions)


}

trait LB {
  val protocol = CinemaProtocols.loadBalancer

}