package homework6

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class GetMoviesViaAPI extends Simulation {

  val scn = scenario("Get movies via API")
    .exec(http("Get first movie")
      .get("/movies/1")
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[Int].is(1)))
    .exec(http("Get second movie")
      .get("/movies/2")
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[Int].is(2)))
    .exec(http("Get third movie")
      .get("/movies/3")
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[Int].is(3)))
    .exec(http("Get fourth movie")
      .get("/movies/4")
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[Int].is(4)))
    .exec(http("Get fifth movie")
      .get("/movies/5")
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[Int].is(5)))

  val assertion = Seq(
    global.responseTime.mean.lt(1000),
    global.successfulRequests.percent.gte(99),
    details("Get third movie").responseTime.percentile3.is(800),
    forAll.responseTime.mean.lt(1200)
  )

  val injectionProfile = Seq(
    constantUsersPerSec(15).during(5 minutes),
    rampUsersPerSec(2).to(15).during(8 minutes),
    constantUsersPerSec(7).during(8 minutes)
  )

  val protocool = http.baseURL("http://10.10.0.101:9000")

  setUp(
    scn.inject(injectionProfile))
    .protocols(protocool)
    .assertions(assertion)
}
