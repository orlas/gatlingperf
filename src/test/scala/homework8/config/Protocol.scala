package homework8.config

import io.gatling.core.Predef._
import io.gatling.http.Predef.http

object Protocol {

  val loadBalancerAddress = http.baseURL(Config.loadBalancerUrl)

  val gatewayAddressWithHeader = http.baseURL(Config.gatewayUrl)
    .header(Config.gatewayAuthorizationHeaderName, Config.gatewayAuthorizationHeaderValue)

  val api1Address = http.baseURL(Config.api1Url)

  val api2Address = http.baseURL(Config.api2Url)

  val api3Address = http.baseURL(Config.api3Url)


}
