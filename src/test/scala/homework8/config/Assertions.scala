package homework8.config

import io.gatling.core.Predef._

object Assertions {
  val globalAssertions = Seq(global.responseTime.max.lt(1000))

  val slowAssertion = Seq(
    global.responseTime.max.lt(5000),
    global.responseTime.percentile4.lt(3000)
  )

  val globalResponseAssertionSequences = Seq(
    global.responseTime.mean.lt(1000),
    global.successfulRequests.percent.gte(99),
    forAll.responseTime.mean.lt(1200)
  )

  def assertionsForLessArgThanRqFails(respTime: Int, failRqProcent: Int) = Seq(
    global.responseTime.mean.lt(respTime),
    forAll.failedRequests.percent.gt(failRqProcent)
  )

  def scaledAssertions(factor: Double = 1.0) = Seq(
    global.responseTime.max.lt((1000 * factor).toInt),
    global.responseTime.percentile3.lt((500 * factor).toInt),
    global.responseTime.mean.lt((400 * factor).toInt)
  )

  





}
