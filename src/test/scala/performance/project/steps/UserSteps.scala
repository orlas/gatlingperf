package performance.project.steps

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import performance.project.model.AuthorizationRequest

object UserSteps extends StepBase {

  def authenticateUser(request: AuthorizationRequest) = http("Auth user")
    .post("/authenticate")
    .body(jsonBodyFromObject(request))
    .asJSON
    .check(status.is(200))
    .check(jsonPath("$.token").exists.saveAs("userToken"))

  def authenticateUserAlt = http("Auth user")
    .post("/authenticate")
    .body(jsonBodyFromObject(AuthorizationRequest("${username}", "${password}")))
    .asJSON
    .check(status.is(200))
    .check(jsonPath("$.token").exists.saveAs("userToken"))

}
