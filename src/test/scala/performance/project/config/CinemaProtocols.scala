package performance.project.config

import io.gatling.core.Predef._
import io.gatling.http.Predef.http

object CinemaProtocols {

  val loadBalancer = http.baseURL("http://api-lb.softwareqa.pl")
}
