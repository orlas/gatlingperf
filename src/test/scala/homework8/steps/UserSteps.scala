package homework8.steps

import homework8.config.ResourcesPath
import homework8.model.{AuthorizationRequest, User}
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.reflect.io.File

object UserSteps extends StepsBase with GetLoginAndPassword {

  def addUser(user: => User) = http("Add user")
    .post("/users")
    .body(StringBody(session => writeJsonFromObject(user)))
    .check(userLogin, userPassword)
    .check(status.is(201))

  def addUserAndSaveUserParams(user: => User, userNameAttribute: String) = {
    exec(session => session.set(userNameAttribute, user))
      .exec(http("Add user and save request parameters")
        .post("/users")
        .body(StringBody(session => writeJsonFromObject(session(userNameAttribute).as[User])))
        .asJSON
      )
  }

  def authenticateUserAndSaveTokenAndId(userNameAttribute: String) = {
    exec(http("Authenticate user")
      .post("/authenticate")
      .body(StringBody(session => writeJsonFromObject(AuthorizationRequest
      (
        session(userNameAttribute).as[User].username,
        session(userNameAttribute).as[User].password
      ))))
      .asJSON
      .check(jsonPath("$.token").exists.saveAs("userToken"))
      .check(jsonPath("$.id").exists.saveAs("userId"))
      .check(status.is(200)))
  }

  def authenticateUserAndSaveToFile(userAttrName: String, tokenAttrName: String) = {
    exec(http("Authenticate user")
      .post("/authenticate")
      .body(
        StringBody(
          session => writeJsonFromObject(
            AuthorizationRequest(
              session(userAttrName).as[User].username,
              session(userAttrName).as[User].password
            )
          )
        )
      ).asJSON
      .check(jsonPath("$.token").exists.saveAs(tokenAttrName))
      .check(jsonPath("$.id").exists.saveAs("userId")))
      .exec(session => {
        File("userLoginParamsWithToken.txt")
          .createFile()
          .appendAll(s"" +
            s"${session(userAttrName).as[User].username}, " +
            s"${session(userAttrName).as[User].password}, " +
            s"${session("userId")}, " +
            s"${session(tokenAttrName).as[String]}\n")
        session
      })
  }

  def payInUserWallet(userId: => String, userToken: => String, money: => BigDecimal) =
    http("Charge user wallet")
      .get("/users/${" + userId + "}/wallet/payin?value=${" + money + "}")
      .header("Http-Auth-Token", "${userToken}")
      .check(status.is(200))
      .check(jsonPath("$.value").ofType[BigDecimal].is(money))


  def getUserWalletValue(userId: => String, userToken: => String) = http("get user wallet value")
    .get(s"/users/${userId}/wallet")
    .header("Http-Auth-Token", "${userToken}")
    .check(jsonPath("$.value").exists.saveAs("userWalletValue"))

  def getUserOrdersAndSaveToFile(userId: => String, userToken: => String) = {
    exec(
      http("Get  user orders")
        .get("/users/${" + userId + "}/orders")
        .header("Http-Auth-Token", "${userToken}")
        .check(status.is(200))
        .check(jsonPath("$.value").exists.saveAs("ordersValue")
        )
    )
      .exec(session => {
        File("userOrders.txt").createFile().appendAll(
          s"${session(userId)}, " +
            s"${session(userToken)}, " +
            s"${session("ordersValue")}\n")
        session
      })

  }
}

trait GetLoginAndPassword {

  val userLogin = jsonPath("$.username").transform(
    username => File(ResourcesPath.userDataPath).appendAll(username)).exists
  val userPassword = jsonPath("$.password").transform(
    password => File(ResourcesPath.userDataPath).appendAll(password)).exists
}