package homework8.config

import com.typesafe.config.ConfigFactory

object Config extends App {

  val configuration = ConfigFactory.load()

  //--------------LoadBalancerConfig-----------------------------------------------------

  val loadBalancerUrl = configuration.getString("loadBalancer.host") + ":" + configuration.getInt("loadBalancer.port").toString

  //--------------GatewayConfig----------------------------------------------------------

  val gatewayUrl = configuration.getString("gateway.host") + ":" + configuration.getInt("gateway.port").toString

  val gatewayAuthorizationHeaderName = configuration.getString("gateway.authorizationHeader")

  val gatewayAuthorizationHeaderValue = configuration.getString("gateway.authorizationValue")

  //---------------Api-s Config-----------------------------------------------------------

  val api1Url = configuration.getString("api1.host") + ":" + configuration.getInt("api1.port").toString

  val api2Url = configuration.getString("api2.host") + ";" + configuration.getInt("api2.port").toString

  val api3Url = configuration.getString("api3host") + ":" + configuration.getInt("api3.port").toString


}
