package com.gatling.workshop

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import scala.concurrent.duration._

class LoginUserSimulation extends Simulation {


  val userAuthenticationJson =
    """{
      |  "username": "${username}",
      |  "password": "${password}"
      |}""".stripMargin

  val iteratorFeeder = Iterator.continually(Map(
    "username" -> "shadow",
    "password" -> "asd123"
  ))

  val arrayFeeder = Array[Map[String, Any]](
    Map("username" -> "shadow", "password" -> "asd123"),
    Map("username" -> "hanzo", "password" -> "plt645"),
    Map("username" -> "rosen", "password" -> "uyj658"),
    Map("username" -> "lion", "password" -> "oku654"),
    Map("username" -> "dolly", "password" -> "vgt12f")
  )

  val csvFeeder = csv("src/test/resources/users.csv").circular

  val jsonFeeder = jsonFile("src/test/resources/users.json").circular

  val jsonUrlFeeder = jsonUrl("http://softwareqa.pl/perf/resources/users.json").circular

  val dbFeeder = jdbcFeeder("jdbc:postgresql://192.168.0.22/cinema_db", "cinema", "cinemapass", "SELECT * FROM users")

  val loginScenario = scenario("Login user")
    //.exec(s => s.set("username", "shadow").set("password", "asd123"))
    .feed(dbFeeder)
    .exec(
      http("Auth user")
        .post("/authenticate")
        .body(StringBody(userAuthenticationJson))
        .asJSON
        .check(status.is(200))
    )

  val ip = Seq(constantUsersPerSec(15).during(10 seconds))

  val protocol = http.baseURLs(
    "http://10.10.0.101:9000")

  setUp(loginScenario.inject(ip).protocols(protocol))
}
