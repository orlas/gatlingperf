package performance.project.config

import io.gatling.core.Predef.{constantUsersPerSec, _}

import scala.concurrent.duration._

object CinemaProfiles {
  val oneDuringOne = constantUsersPerSec(1).during(1)

  def constantWithWarmUp(users: Int, duration: Int) = Seq(
    rampUsersPerSec(1).to(users).during(60 seconds),
    constantUsersPerSec(users).during(duration seconds)
  )

}
