package performance.project.steps

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import performance.project.model.Movie


object MovieSteps extends StepBase {

  val getMovies = http("Get movies")
    .get("/movies")
    .check(status.is(200))

  def getMovie(id: => Int) = http("Get movies")
    .get(s"/movies/${id}")
    .check(status.is(200))
    .check(bodyString.transform(s => readJsonBodyAs[Movie](s).id.get == id).is(true))

  def getMoviesPage(page: => Int) = http("Get movies page")
    .get(s"/movies?page=${page}")
    .check(status.is(200))

  def addMovie(movie: => Movie) = http("Add movie")
    .post("/movies")
    .body(jsonBodyFromObject(movie))
    .asJSON
    .check(status.is(201))


}
