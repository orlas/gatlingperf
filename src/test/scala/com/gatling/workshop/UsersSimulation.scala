package com.gatling.workshop

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._
import scala.util.Random

class UsersSimulation extends Simulation {

  def usersData =
    """{
      |    "firstname":"abc",
      |    "lastname":"abv",
      |    "email": "${userEmail}",
      |    "username":"${userName}",
      |      "password":"aaa123",
      |    "city":"bkkkds",
      |    "address":"asadasas",
      |    "birthdate":"2000-12-12"
      |}""".stripMargin

  //Expression[A] <==> Session => A
  //SessionFunction --> Session => Session

  val scn = scenario("Add new user")
    .exec(
      sessionFunction = session => session
        .set("userName", Random.nextInt(10000))
        .set("userEmail", s"${Random.nextInt(100000)}@gmail.com")
    )
    .exec(http("Add user")
      .post("/users")
      .body(StringBody(usersData)).asJSON
      .check(
        status.is(201)
      )
      .check(jsonPath("$.id").exists.saveAs("userId"))
    ).exec(
    http("Auth user")
      .post("/authenticate")
      .body(StringBody(
        s"""{
           |"username":"$${userName}",
           |"password":"aaa123"
           |}""".stripMargin))
      .asJSON
      .check(status.is(200))
      .check(jsonPath("$.token").exists.saveAs("userToken"))
  )
    .exec(
      http("Check wallet")
        .get("/users/${userId}/wallet")
        .header("Http-Auth-Token", "${userToken}")
        .check(status.is(200))
        .check(jsonPath("$.value").ofType[Double].is(0.0))
        .check(jsonPath("$.value").ofType[Double].is(getInt(s => s("userId").as[Int])))
        .check(status.is("$.userId"))
        .check(status.is("$.userId.size"))
    )
    .exec(
      session => {
        println(session.attributes.mkString(","))
        //session.set()
        session
      }

    )

  def getInt(f: Session => Int) = 0.0

  val ip = Seq(constantUsersPerSec(1).during(5 second)
    //    rampUsersPerSec(1).to(15).during(15 seconds),
    //    constantUsersPerSec(15).during(30 seconds)
  )

  val protocol = http.baseURLs(
    "http://10.10.0.101:9000")

  setUp(scn.inject(ip).protocols(protocol))

}
